import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int element = sc.nextInt();
        int arr[] = new int[element];
        int uniqeArr[] = new int[element];
        int uniqeElement = 0;
        for(int i = 0;i < arr.length;i++){
            System.out.print("Element "+i+": ");
            arr[i] = sc.nextInt();
            int index = -1;
            for(int j = 0;j < uniqeElement;j++){
                if(uniqeArr[j]==arr[i]){
                    index = j;
                }
            }
            if(index<0){
                uniqeArr[uniqeElement] = arr[i];
                uniqeElement++;
            }
        }
        System.out.print("All number: ");
        for(int i = 0;i < uniqeElement;i++){
            System.out.print(uniqeArr[i]+" ");
        }
        sc.close(); 
    }
}
