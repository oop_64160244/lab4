import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sum = 0;
        int min = 0;
        int max = 0;
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
            sum = sum + arr[i];
            if (i == 0) {
                min = arr[i];
            }
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = " + sum);
        double avg = (double) sum / arr.length;
        System.out.println("avg = " + avg);
        System.out.println("min = " + min);
        System.out.println("max = " + max);
        sc.close();
    }
}
