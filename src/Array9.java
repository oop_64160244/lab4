import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arr[] = new int[3];
        for (int i = 0; i < arr.length; i++) {
            System.out.print("Please input arr[" + i + "]: ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.print("Please input search value: ");
        int search = sc.nextInt();
        int index = 0 ;
        for (int i = 0; i < arr.length; i++) {
            if (search == arr[i]){
                index = i;
                System.out.print("found at index: "+i);
                break;
            }else if(search != arr[i]){
                index = -1;
            }
        }
        if(index == -1){
            System.out.print("not found");
        }
        sc.close();      
    }
}

